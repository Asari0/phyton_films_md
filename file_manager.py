# Your code goes here.
import json


def count(filename):
    """Ouvre le fichier JSON, et compte le nombre d'éléments présents dans la structure."""
    with open(filename) as file:
        data = json.load(file)
    length = len(data)
    return length


def written_by(filename, name):
    """Ouvre le fichier JSON, et cherche les enregistrements écrits par Vince Gilligan"""
    with open(filename) as file:
        data = json.load(file)
    films = []
    for i in range(len(data)):
        if "Writer" not in data[i]:
            i += 1
        elif data[i]["Writer"] == "N/A":
            i += 1
        if name in data[i]["Writer"]:
            films.append(data[i])
    return films


def longest_title(filename):
    """Ouvre le fichier JSON, et cherche le film avec le nom le plus long"""
    with open(filename) as file:
        data = json.load(file)
    title = " "
    ref = 0
    for i in range(len(data)):
        if len(data[i]["Title"]) > len(title):
            title = data[i]["Title"]
            ref = i
    print(data[ref]["Title"])
    return data[ref]


def best_rating(filename):
    """Ouvre le fichier JSON, et cherche l'enregistrement avec le meilleur imdbRating """
    with open(filename) as file:
        data = json.load(file)
    rating = 0
    ref = 0
    for i in range(len(data)):
        if 'imdbRating' not in data[i]:
            i += 1
        elif data[i]["imdbRating"] == "N/A":
            i += 1
        elif float(data[i]["imdbRating"]) > rating:
            rating = float(data[i]["imdbRating"])
            ref = i
    print(data[ref]["imdbRating"])
    return data[ref]


def latest_film(filename):
    """Ouvre le fichier JSON, et cherche l'enregistrement avec
    le Year le plus récent, retourne le nom du film concerne"""
    with open(filename) as file:
        data = json.load(file)
    start = 0
    ref = 0
    for i in range(len(data)):
        if 'Year' not in data[i]:
            i += 1
        elif data[i]["Year"] == "N/A":
            i += 1
        elif data[i]['Year'][(len(data[i]['Year']) - 1)] != type(start):
            if int(data[i]["Year"][:4]) >= start:
                start = int(data[i]["Year"][:4])
                ref = i
        elif int(data[i]['Year'][len(data[i]['Year']) - 4:]) > start:
            start = int(data[i]['Year'][len(data[i]['Year']) - 4:])
            ref = i
    print(data[ref]["Title"])
    return data[ref]["Title"]


def find_per_genre(filename, name):
    """Ouvre le fichier JSON, et cherche les enregistrements ayant le genre Fantasy"""
    with open(filename) as file:
        data = json.load(file)
    genre_film = []
    for i in range(len(data)):
        if 'Genre' not in data[i]:
            i += 1
        elif data[i]["Genre"] == "N/A":
            i += 1
        elif name in data[i]['Genre']:
            genre_film.append(data[i])
    print(genre_film)
    return genre_film


def released_after(filename, date):
    """Ouvre le fichier JSON, et compte le nombre d'éléments ayant une release date >= Nov 2015 """
    with open(filename) as file:
        data = json.load(file)
    after_date = []
    for i in range(len(data)):
        if "Released" not in data[i]:
            i += 1
        elif data[i]["Released"] == "N/A":
            i += 1
        elif int(data[i]['Released'][len(data[i]['Released']) - 4:]) == int(date[len(date) - 4:]):
            if data[i]['Released'][3:6] == 'Nov':
                if int(data[i]['Released'][0:2]) > int(date[0:2]):
                    after_date.append(data[i])
        elif int(data[i]['Released'][len(data[i]['Released']) - 4:]) > int(date[len(date) - 4:]):
            after_date.append(data[i])
    return after_date
